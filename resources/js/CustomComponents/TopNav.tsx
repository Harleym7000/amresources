import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import {Navbar} from "react-bootstrap";

export default function TopNav() {

    return (
        <>
            <Navbar
                collapseOnSelect
                expand="lg"
                className="shadow-lg"
                data-testid="navBarTop">
                <Container>
                    <Navbar.Brand href="#home">
                        <img
                            alt="logo"
                            src="/img/logo.png"
                            width="140"
                            height="90"
                        />{' '}
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto"></Nav>
                        <Nav>
                            <Nav.Link href="/"
                                      className="nav-links me-5"
                            >
                                Home
                            </Nav.Link>
                            <Nav.Link href="/about"
                                      className="nav-links me-5"
                            >
                                About
                            </Nav.Link>
                            <Nav.Link href="/services"
                                      className="nav-links me-5"
                            >
                                Services
                            </Nav.Link>
                            <Nav.Link href="/news"
                                      className="nav-links me-5"
                            >
                                News
                            </Nav.Link>
                            <Nav.Link href="/contactUs"
                                      className="nav-links me-5"
                            >
                                Contact Us
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    );
}
