import TopNav from "../CustomComponents/TopNav";
import Footer from "../CustomComponents/Footer";
import {useState} from "react";

export default function About() {

    const [clickCount, setClickCount] = useState(0);
    const handleClick = () => {
        setClickCount(clickCount + 1);
    }
    return (
        <>
            <TopNav/>
            <h1>ABOUT PAGE</h1>
            <Footer/>
        </>
    );
}
