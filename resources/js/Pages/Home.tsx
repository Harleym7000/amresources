import TopNav from "../CustomComponents/TopNav";
import Footer from "../CustomComponents/Footer";

export default function Home() {
    return (
        <>
            <TopNav/>
            <Footer/>
        </>
    );
}
