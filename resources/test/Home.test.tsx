import {render} from "@testing-library/react";
import Home from "../js/Pages/Home";

describe ("Home tests", () => {

    let renderComponent: () => any;

    beforeEach(() => {
        renderComponent = () => {
            return render(
                <Home/>
            )
        }
    });

    test("renders navbar", () => {
        const {getByTestId} = renderComponent();

        expect(getByTestId("navBarTop")).toBeTruthy();
        expect(getByTestId("navBarTop").textContent).toContain("Home");
        expect(getByTestId("navBarTop").textContent).toContain("About");
        expect(getByTestId("navBarTop").textContent).toContain("Services");
        expect(getByTestId("navBarTop").textContent).toContain("News");
        expect(getByTestId("navBarTop").textContent).toContain("Contact Us");
    });

    test("has a footer component", () => {
        const {getByTestId} = renderComponent();

        expect(getByTestId("footer")).toBeTruthy();
        expect(getByTestId("footer").textContent).toBe("AM Resources");
    });
});
